add $s6, $s5, $zero
addi $s6, $s5, 5
sw $s6, 1($s6)
lw $s6, 1($s6)
lui $s7, 8
sra $s5, $s6, 1
srl $s5, $s6, 1
sub $s5, $s6, $zero
and $s5, $s6, $s5
slt $s5, $s6, $s5
slti $s5, $s6, 7
andi $s5, $s6, 6
or $s5, $s6, $s5
ori $s5, $s6, 6
nor $s5, $s6, $s5
sll $s5, $s6, 2
srl $s5, $s6, 2
