import math
import array

MEMORY_SIZE = 2**16

#Register class
registers = {"$zero":0,
                "$at":1,
                "$v0":2,
                "$v1":3,
                "$a0":4,
                "$a1":5,
                "$a2":6,
                "$a3":7,
                "$t0":8,
                "$t1":9,
                "$t2":10,
                "$t3":11,
                "$t4":12,
                "$t5":13,
                "$t6":14,
                "$t7":15,
                "$s0":16,
                "$s1":17,
                "$s2":18,
                "$s3":19,
                "$s4":20,
                "$s5":21,
                "$s6":22,
                "$s7":23,
                "$t8":24,
                "$t9":25,
                "$k0":26,
                "$k1":27,
                "$gp":28,
                "$sp":29,
                "$fp":30,
                "$ra":31
                }

class Register:
    def __init__(self, value,binaryLocation):
        self.value = value
        self.binaryLocation = binaryLocation
         
#Declaring registers
registerList = []
for x in range(32):
    registerList.append(Register(0,"{0:05b}".format(x)))

#Operations    
def printOperation(operation, storage, operand1, operand2):
    print("Performed operation " + operation + " on operands " + operand1 + " and " + operand2 + " result stored in " + storage)
    
labels = {}
code = []
memory = array.array('i', (0 for i in range(MEMORY_SIZE)))

def parseParam(name):
    try:
        return registerList[registers[name]]
    except KeyError:
        try: 
            return int(name)
        except ValueError:
            return name

def readCode():
    
    codeFile = open("codeFile.txt","r")
    lineIndex = 0
    currentLine = []
    instantOperations = ["addi","andi","ori","slti"]
    
    while True:
        currentLine.append(codeFile.readline().strip())
        if currentLine[lineIndex] == "" or currentLine[lineIndex] == "\n":
            break
        else:
            line = currentLine[lineIndex].split(":")
            arguments = ""
            if len(line) > 1:
                labels[line[0]] = lineIndex
                arguments = line[1].strip().split(" ")
            else:
                arguments = line[0].split(" ")

            arguments = list(filter(lambda a: a != '', arguments))

            if (len(arguments) == 1 and arguments[0].strip() == "") or len(arguments) == 0:
                break

            arguments[1] = arguments[1].replace(",","")
            arguments[1] = arguments[1].replace("\n","")

            if len(arguments) > 2:
                arguments[2] = arguments[2].replace(",","")
                arguments[2] = arguments[2].replace("\n","")
                arguments[2] = arguments[2].replace(")","")
                arguments[2] = arguments[2].replace("("," ")
            
            if len(arguments) > 3:
                arguments[3] = arguments[3].replace(",","")
                arguments[3] = arguments[3].replace("\n","")
                if arguments[0] in instantOperations:
                    value = int(arguments[3])
                    valueBinary = "{0:016b}".format(value)
            
            code.append(arguments)
            lineIndex += 1

def executeCode():
    i = 0
    while i < len(code):
        arguments = code[i]
        i+=1
        
        print('{:24s}'.format(" ".join(arguments) + ":"), end="")
        #printOperation(arguments[0],arguments[1],arguments[2],arguments[3])
        p1 = parseParam(arguments[1])
        p2 = parseParam(arguments[2]) if len(arguments) > 2 else None
        p3 = parseParam(arguments[3]) if len(arguments) > 3 else None
        
        p1value = p1.value if isinstance(p1, Register) else None
        p2value = p2.value if isinstance(p2, Register) else None
        p3value = p3.value if isinstance(p3, Register) else None

        if arguments[0] == "add":
            p1.value = p2.value + p3.value
            print(arguments[1] + " = " + str(p2value) + " + " + str(p3value) + " = " + str(p1.value))
        elif arguments[0] == "addi":
            p1.value = p2.value + p3
            print(arguments[1] + " = " + str(p2value) + " + " + str(p3) + " = " + str(p1.value))  
        elif arguments[0] == "sub":
            p1.value = p2.value - p3.value
            print(arguments[1] + " = " + str(p2value) + " - " + str(p3value) + " = " + str(p1.value))
        elif arguments[0] == "and":
            p1.value = p2.value & p3.value
            print(arguments[1] + " = " + "{0:032b}".format(p2value) + " & " + "{0:032b}".format(p3value) + " = " + "{0:032b}".format(p1.value))
        elif arguments[0] == "andi":
            p1.value = p2.value & p3
            print(arguments[1] + " = " + "{0:032b}".format(p2value) + " & " + "{0:032b}".format(p3) + " = " + "{0:032b}".format(p1.value))
        elif arguments[0] == "or":
            p1.value = p2.value | p3.value
            print(arguments[1] + " = " + "{0:032b}".format(p2value) + " | " + "{0:032b}".format(p3value) + " = " + "{0:032b}".format(p1.value))
        elif arguments[0] == "ori":
            p1.value = p2.value | p3
            print(arguments[1] + " = " + "{0:032b}".format(p2value) + " | " + "{0:032b}".format(p3) + " = " + "{0:032b}".format(p1.value))
        elif arguments[0] == "nor":
            p1.value = ~(p2.value | p3.value)
            print(arguments[1] + " = ~(" + "{0:032b}".format(p2value) + " | " + "{0:032b}".format(p3value) + ") = " + "{0:032b}".format(p1.value))
        elif arguments[0] == "sll":
            p1.value = p2.value << p3
            print(arguments[1] + " = " + "{0:032b}".format(p2value) + " << " + "{0:032b}".format(p3) + " = " + "{0:032b}".format(p1.value))
        elif arguments[0] == "srl":
            p1.value = (p2.value % 0x100000000) >> p3
            print(arguments[1] + " = " + "{0:032b}".format(p2value) + " >> " + "{0:032b}".format(p3) + " = " + "{0:032b}".format(p1.value))
        elif arguments[0] == "sra":
            p1.value = p2.value >> p3
            print(arguments[1] + " = " + "{0:032b}".format(p2value) + " >> " + "{0:032b}".format(p3) + " = " + "{0:032b}".format(p1.value))
        elif arguments[0] == "slt":
            p1.value = 1 if p2.value < p3.value else 0
            print(arguments[1] + " = 1 if " + str(p2value) + " < " + str(p3value) + " else 0 = " + str(p1.value))
        elif arguments[0] == "slti":
            p1.value = 1 if p2.value < p3 else 0
            print(arguments[1] + " = 1 if " + str(p2value) + " < " + str(p3) + " else 0 = " + str(p1.value))
        elif arguments[0] == "lui":
            p1.value = p2 << 16
            print(arguments[1] + " = " + "{0:032b}".format(p2) + " << 16 = " + "{0:032b}".format(p1.value))
        elif arguments[0] == "beq":
            i = labels[p3] if p1.value == p2.value else i
            print("i = " + p3 + " if " + str(p1value) + " == " + str(p2value) + " else i = " + str(i))
        elif arguments[0] == "bne":
            i = labels[p3] if p1.value != p2.value else i
            print("i = " + p3 + " if " + str(p1value) + " != " + str(p2value) + " else i = " + str(i))
        elif arguments[0] == "sw":
            splitted = p2.split(" ")
            pos = parseParam(splitted[1]).value + int(splitted[0])
            
            memory[pos] = p1.value
            print("memory[" + str(pos) + "] = " + str(p1.value))
        elif arguments[0] == "lw":
            splitted = p2.split(" ")
            pos = parseParam(splitted[1]).value + int(splitted[0])
            
            p1.value = memory[pos]
            print(arguments[1] + " = memory[" + str(pos) + "] = " + str(p1.value))
        elif arguments[0] == "j":
            i = labels[p1]
            print("i = " + p1 + " = " + str(i))


def saveCodeBinary():
    binaryFile = open("binaryFile.txt", "w+")

    i = 0
    while i < len(code):
        arguments = code[i]
        i += 1

        # print(" ".join(arguments))

        p1 = parseParam(arguments[1])
        p2 = parseParam(arguments[2]) if len(arguments) > 2 else None
        p3 = parseParam(arguments[3]) if len(arguments) > 3 else None

        valueBinary = "{0:016b}".format(p3) if isinstance(p3, int) else None

        if arguments[0] == "add":
            binaryFile.write("000000" + p2.binaryLocation + p3.binaryLocation + p1.binaryLocation + "00000100000\n")
        elif arguments[0] == "addi":
            binaryFile.write("001000" + p2.binaryLocation + p1.binaryLocation + valueBinary + "\n")
        elif arguments[0] == "sub":
            binaryFile.write("000000" + p2.binaryLocation + p3.binaryLocation + p1.binaryLocation + "00000100010\n")
        elif arguments[0] == "and":
            binaryFile.write("000000" + p2.binaryLocation + p3.binaryLocation + p1.binaryLocation + "00000100100\n")
        elif arguments[0] == "andi":
            binaryFile.write("001100" + p2.binaryLocation + p1.binaryLocation + valueBinary + "\n")
        elif arguments[0] == "or":
            binaryFile.write("000000" + p2.binaryLocation + p3.binaryLocation + p1.binaryLocation + "00000100101\n")
        elif arguments[0] == "ori":
            binaryFile.write("001101" + p2.binaryLocation + p1.binaryLocation + valueBinary + "\n")
        elif arguments[0] == "nor":
            binaryFile.write("000000" + p2.binaryLocation + p3.binaryLocation + p1.binaryLocation + "00000100111\n")
        elif arguments[0] == "sll":
            binaryFile.write("00000000000" + p2.binaryLocation + p1.binaryLocation + "{0:05b}".format(p3) + "000000\n")
        elif arguments[0] == "srl":
            binaryFile.write("00000000000" + p2.binaryLocation + p1.binaryLocation + "{0:05b}".format(p3) + "000010\n")
        elif arguments[0] == "slt":
            binaryFile.write("000000" + p2.binaryLocation + p3.binaryLocation + p1.binaryLocation + "00000101010\n")
        elif arguments[0] == "slti":
            binaryFile.write("001010" + p2.binaryLocation + p1.binaryLocation + valueBinary + "\n")
        elif arguments[0] == "sra":
            binaryFile.write("00000000000" + p2.binaryLocation + p1.binaryLocation + "{0:05b}".format(
                p3) + "000011\n")
        elif arguments[0] == "lui":
            binaryFile.write("00111100000" + p1.binaryLocation + "{0:016b}".format(p2) + "\n")
        elif arguments[0] == "beq":
            binaryFile.write("000100" + p1.binaryLocation + p2.binaryLocation + "{0:016b}".format(labels[p3] - int(i)) + "\n")
        elif arguments[0] == "bne":
            binaryFile.write("000101" + p1.binaryLocation + p2.binaryLocation + "{0:016b}".format(labels[p3] - int(i)) + "\n")
        elif arguments[0] == "j":
            binaryFile.write ("000010" + "{0:026b}".format(labels[p1] - int(i)) + "\n")
        elif arguments[0] == "lw":
            splitted = p2.split(" ")
            binaryFile.write("100011" + parseParam(splitted[1]).binaryLocation + p1.binaryLocation + "{0:016b}".format(int(splitted[0])) + "\n")
        elif arguments[0] == "sw":
            splitted = p2.split(" ")
            binaryFile.write("101011" + parseParam(splitted[1]).binaryLocation + p1.binaryLocation + "{0:016b}".format(int(splitted[0])) + "\n")
        
def main():
    print("Registers before execution:")
    for key in registers.keys():
        print(str(key)+": "+ str(registerList[registers[key]].value),end="\t")
    print("\n")
    readCode()
    executeCode()
    saveCodeBinary()
    print("")
    print("Registers after execution:")
    for key in registers.keys():
        print(str(key)+": "+ str(registerList[registers[key]].value),end="\t")
                

if(__name__ == "__main__"):
    main()
