#Register class
registers = {"$zero":0,
                "$at":1,
                "$v0":2,
                "$v1":3,
                "$a0":4,
                "$a1":5,
                "$a2":6,
                "$a3":7,
                "$t0":8,
                "$t1":9,
                "$t2":10,
                "$t3":11,
                "$t4":12,
                "$t5":13,
                "$t6":14,
                "$t7":15,
                "$s0":16,
                "$s1":17,
                "$s2":18,
                "$s3":19,
                "$s4":20,
                "$s5":21,
                "$s6":22,
                "$s7":23,
                "$t8":24,
                "$t9":25,
                "$k0":26,
                "$k1":27,
                "$gp":28,
                "$sp":29,
                "$fp":30,
                "$ra":31
                }

class Register:
    def __init__(self, value,binaryLocation):
        self.value = value
        self.binaryLocation = binaryLocation
         
#Declaring registers
registerList = []
for x in range(32):
    registerList.append(Register(0,"{0:05b}".format(x)))

#Operations    
def printOperation(operation, storage, operand1, operand2):
    print("Performed operation " + operation + " on operands " + operand1 + " and " + operand2 + " result stored in " + storage)
    
labels = {}
code = []
pipelineTrace = []

def parseParam(name):
    try:
        return registerList[registers[name]]
    except KeyError:
        try: 
            return int(name)
        except ValueError:
            return name

def readCode():
    codeFile = open("codeFile.txt","r")
    lineIndex = 0
    currentLine = []
    instantOperations = ["addi","andi","ori","slti"]
    while True:
        currentLine.append(codeFile.readline().strip())
        if currentLine[lineIndex] == "" or currentLine[lineIndex] == "\n":
            break
        else:
            line = currentLine[lineIndex].split(":")
            arguments = ""
            if len(line) > 1:
                labels[line[0]] = lineIndex
                arguments = line[1].strip().split(" ")
            else:
                arguments = line[0].split(" ")

            arguments = list(filter(lambda a: a != '', arguments))
            
            if (len(arguments) == 1 and arguments[0].strip() == "") or len(arguments) == 0:
                break

            arguments[1] = arguments[1].replace(",","")
            arguments[1] = arguments[1].replace("\n","")

            if len(arguments) > 2:
                arguments[2] = arguments[2].replace(",","")
                arguments[2] = arguments[2].replace("\n","")
            
            if len(arguments)>3:
                arguments[3] = arguments[3].replace(",","")
                arguments[3] = arguments[3].replace("\n","")
                if arguments[0] in instantOperations:
                    value = int(arguments[3])
                    valueBinary = "{0:016b}".format(value)
            
            code.append(arguments)
            lineIndex += 1

def executeCode():
    i = 0
    while i < len(code):
        pipelineTrace.append(i)
        arguments = code[i]
        i+=1
        
        print('{:24s}'.format(" ".join(arguments) + ":"), end="")
        #printOperation(arguments[0],arguments[1],arguments[2],arguments[3])
        p1 = parseParam(arguments[1])
        p2 = parseParam(arguments[2]) if len(arguments) > 2 else None
        p3 = parseParam(arguments[3]) if len(arguments) > 3 else None

        p1value = p1.value if isinstance(p1, Register) else None
        p2value = p2.value if isinstance(p2, Register) else None
        p3value = p3.value if isinstance(p3, Register) else None

        if arguments[0] == "add":
            p1.value = p2.value + p3.value
            print(arguments[1] + " = " + str(p2value) + " + " + str(p3value) + " = " + str(p1.value))
        elif arguments[0] == "slti":
            p1.value = 1 if p2.value < p3 else 0
            print(arguments[1] + " = 1 if " + str(p2value) + " < " + str(p3) + " else 0 = " + str(p1.value))
        elif arguments[0] == "beq":
            i = labels[p3] if p1.value == p2.value else i
            print("i = " + p3 + " if " + str(p1value) + " == " + str(p2value) + " else i = " + str(i))
        elif arguments[0] == "j":
            i = labels[p1]
            print("i = " + p1 + " = " + str(i))

def printPipeline():
    from texttable import Texttable

    table = [["Label", "Command"]]
    for line in code:
        table.append([""," ".join(line)])

    itemsDictionary = {
        "F":"D",
        "D":"E",
        "E":"M",
        "M":"W",
        "W":""
    }

    pipelineTrace.append(-1)
    pipelineTrace.append(-1)
    pipelineTrace.append(-1)
    pipelineTrace.append(-1)

    index = 1
    for traceLine in pipelineTrace:
        first = True
        rowIndex = -1
        for row in table:
            if first:
                first = False
                row.append(index)
                index+=1
            else:
                try:
                    row.append(itemsDictionary[row[-1]])
                except KeyError:
                    if traceLine == rowIndex:
                        row.append("F")
                    else:
                        row.append("")

            rowIndex+=1
    
    for label in labels.items():
        row = []
        if(label[1]+1 == len(table)):
            row = ['']*(index+1)
            table.append(row)
        else:
            row = table[label[1]+1]

        row[0] = label[0]

    t = Texttable()
    t.add_rows(table)
    print(t.draw())

def main():
    print("Registers before execution:")
    for key in registers.keys():
        print(str(key)+": "+ str(registerList[registers[key]].value),end="\t")
    print("\n")
    readCode()
    executeCode()
    printPipeline()
    print("")
    print("Registers after execution:")
    for key in registers.keys():
        print(str(key)+": "+ str(registerList[registers[key]].value),end="\t")
                
if(__name__ == "__main__"):
    main()
